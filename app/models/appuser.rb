class Appuser < ActiveRecord::Base
	has_secure_password
    validates_uniqueness_of :email
    validates :email , :presence => true 
    validates :phone_number , :presence => true 


	has_many :truckers

	def verified_entries_count
		truckers.where(:is_verified => true).count
	end


end
