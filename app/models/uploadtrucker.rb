class Uploadtrucker < ActiveRecord::Base
	include ActiveModel::Model

    attr_accessor :file

	def self.accessible
		listOfAttributes = ["truck_number", "trucker_name", "rc_number", "trucker_phn_no"]
	end

    def initialize(attributes = {})
    	attributes.each { |name, value| send("#{name}=", value) }
  	end

    def persisted?
    	false
  	end

	def self.upload(file)
		spreadsheet = open_spreadsheet(file)
		header = spreadsheet.row(1)		

		(2..spreadsheet.last_row).each do |i|
			row = Hash[[header, spreadsheet.row(i)].transpose]
			truck = Trucker.new

			fields = trucker_fields

			if row[fields["truck_number"]] && !Trucker.exists?(:truck_number => row[fields["truck_number"]])
				truck[:trucker_name] = row[fields["trucker_name"]]
				truck[:truck_number] = row[fields["truck_number"]]
				truck[:trucker_phn_no] = row[fields["trucker_phn_no"]]
				truck[:rc_number] = row[fields["rc_number"]]
				truck[:truck_type] = row[fields["truck_type"]]
				
				truck.save
			end
		end
	end

	def self.open_spreadsheet(file)
		case File.extname(file.original_filename)
		when ".csv" then Roo::Csv.new(file.path, csv_options: {encoding: "iso-8859-1:utf-8"})
		when ".xls" then Roo::Excel.new(file.path, csv_options: {encoding: "iso-8859-1:utf-8"})
		when ".xlsx" then Roo::Excelx.new(file.path, csv_options: {encoding: "iso-8859-1:utf-8"})
		else raise "Unknown file type: #{file.original_filename}"
		end
	end

	#Return Column names of a Trucker to be read from Uploaded File
	def self.trucker_fields
		fields =  {
				"trucker_name" => "Truck Owner",
				"truck_number" => "TruckNo",
				"trucker_phn_no" => "Truck Owner Phone No.",
				"rc_number" => "RC number",
				"truck_type" => "Vehicle Type",
				"has_gps" => "GPS"
			}
		return fields
	end

end
