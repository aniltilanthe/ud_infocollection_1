class Trucker < ActiveRecord::Base
   

    TRUCK_TYPES = ["Canter","Car Carrier","Cement","Container","Low Bed Trailer","Hydraulic Trailer/ODC","Refrigertated","Scooter Body","Trailer","Tipper","Tanker","Truck"]
    belongs_to :appuser , :counter_cache => true
	has_many :drivers ,  dependent: :destroy
	accepts_nested_attributes_for :drivers, limit: 1

	  has_attached_file :rc_attach, :styles => {:thumb => "100x100>"}
	  validates_attachment_content_type :rc_attach, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]
	  
	  has_attached_file :transit_pass_attach, :styles => {:thumb => "100x100>"}
	  validates_attachment_content_type :transit_pass_attach, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]


	  has_attached_file :chassis_number_attach, :styles => {:thumb => "100x100>"}
	  validates_attachment_content_type :chassis_number_attach, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]	  
  
  #specify that the resume is a paperclip file attachment
      

    validates :trucker_name, :presence => true 
	validates :truck_number, :presence => true
	validates :trucker_phn_no, :presence =>true

end
