class UploadtruckersController < ApplicationController

	skip_before_action :verify_authenticity_token

  	def show
  		render template: "uploadtruckers/show"
  	end

  	def submit
  		Uploadtrucker.upload(params[:form][:file])
		  redirect_to root_url, notice: "File uploaded."
	  end

end