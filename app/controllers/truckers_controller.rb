class TruckersController < ApplicationController
   #before_action :
   before_filter :authorize
  before_action :set_trucker , only: [:show, :edit, :update, :destroy]
  #skip_before_action :verify_authenticity_token



  #attr_reader :current_user
  #helper_method :current_user

  private

  def authenticate_request
    @current_user = AuthenticateApiRequest.call(request.headers).result

    render json: { error: 'Not Authorized' }, status: 401 unless @current_user
  end
  # GET /truckers
  # GET /truckers.json

   public

  def index
  if (current_user.email == "aysyal31@gmail.com" || current_user.email == "vishwaas.malik@m.darcl.com" ) 
    @truckers = Trucker.all
  else
    redirect_to '/404.html'

    end


  end

  # GET /truckers/1
  # GET /truckers/1.json
  def show
    @trucker = Trucker.find_by_truck_number(params[:id])

  end

  def truckavailable
    @trucker = Trucker.find_by_truck_number(params[:id])
    if @trucker
      render json: { 'valid' => false}
    else
      render json: { 'valid' => true}
    end
  end

  # GET /truckers/new
  def new
    if !current_user.is_verified 
      redirect_to '/verification'
    else 
    @trucker = Trucker.new
  end

  end

  # GET /truckers/1/edit
  def edit
    if (current_user.email == ("aysyal31@gmail.com"))
     @trucker = Trucker.find(params[:id])
    else 
      redirect_to '/404.html'
    end
  end

  # POST /truckers
  # POST /truckers.json
  def create
    #@trucker = Trucker.where(truck_number: @trucker.truck_number).first_or_create
    truckers = trucker_params ;
    #driver = truckers.delete("drivers"); 
    #@trucker = Trucker.new(truckers)
    # @trucker = Trucker.find_or_initialize_by(truck_number: params[:trucker][:truck_number])

     @trucker = current_user.truckers.new(trucker_params)


     #@trucker.update(trucker_params)
    result= @trucker.save

    #if driver
     # @trucker.drivers.create(driver)
    #end

    respond_to do |format|
      if result
        format.html { redirect_to '/truckers/new', notice: 'Trucker was successfully created.' }
        format.json { render :show, status: :created, location: @trucker }
      else
        format.html { render :new }
        format.json { render json: @trucker.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /truckers/1
  # PATCH/PUT /truckers/1.json
  def update
    @trucker = Trucker.find(params[:id])
    respond_to do |format|
      if @trucker.update(trucker_params)
        format.html { redirect_to @trucker, notice: 'Trucker was successfully updated.' }
        format.json { render :show, status: :ok, location: @trucker }
      else
        format.html { render :edit }
        format.json { render json: @trucker.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /truckers/1
  # DELETE /truckers/1.json
  def destroy
    
    if (current_user.email == ("aysyal31@gmail.com"))
    
      @trucker = Trucker.find(params[:id])
      @trucker.destroy
      respond_to do |format|
        format.html { redirect_to truckers_url, notice: 'Trucker was successfully destroyed.' }
        format.json { head :no_content }
      end
    else 
      redirect_to '/404.html'
    end

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trucker
      #@trucker = Trucker.find(params[:id])
      @trucker = Trucker.find_by_truck_number(params[:truck_number])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def trucker_params
      params.require(:trucker).permit(:trucker_name, :truck_number, :transporter_name, :rc_number, :trucker_phn_no, :has_gps, :insurance, :pref_route, :fitness, :transit_pass, :permit, :emi, :opt_f1, :opt_f2, :opt_f3, 
	:opt_f4, :opt_f5, :is_verified, :route_start, :route_end, :fleet_size, 
	:truck_type,:consignee_name, :transit_pass_attach, :rc_attach, :chassis_number_attach,
	drivers_attributes: [:driver_name, :driver_pnh_no, :license_no])
            #params.require(:trucker).permit(:trucker_name, :truck_number, :rc_number, :license_no, :driver_name, :driver_phn_no, :trucker_phn_no, :has_gps, :insurance, :pref_route, :fitness, :transit_pass, :permit, :emi, :opt_f1, :opt_f2, :opt_f3, :opt_f4, :opt_f5)
    end

  #export truckers data
  public
    def index
      @truckers = Trucker.all
      respond_to do |format|
        format.html
        format.xls { send_data @truckers.to_xls, content_type: 'application/vnd.ms-excel', filename: 'users.xls' }
      end
    end
end
