class ContactUsUsersController < ApplicationController
  before_action :set_contact_us_user, only: [:show, :edit, :update, :destroy]

  # GET /contact_us_users
  # GET /contact_us_users.json
  def index
    @contact_us_users = ContactUsUser.all
  end

  # GET /contact_us_users/1
  # GET /contact_us_users/1.json
  def show
  end

  # GET /contact_us_users/new
  def new
    @contact_us_user = ContactUsUser.new
  end

  # GET /contact_us_users/1/edit
  def edit
  end

  # POST /contact_us_users
  # POST /contact_us_users.json
  def create
    @contact_us_user = ContactUsUser.new(contact_us_user_params)

    respond_to do |format|
      if @contact_us_user.save
        format.html { redirect_to @contact_us_user, notice: 'Contact us user was successfully created.' }
        format.json { render :show, status: :created, location: @contact_us_user }
      else
        format.html { render :new }
        format.json { render json: @contact_us_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contact_us_users/1
  # PATCH/PUT /contact_us_users/1.json
  def update
    respond_to do |format|
      if @contact_us_user.update(contact_us_user_params)
        format.html { redirect_to @contact_us_user, notice: 'Contact us user was successfully updated.' }
        format.json { render :show, status: :ok, location: @contact_us_user }
      else
        format.html { render :edit }
        format.json { render json: @contact_us_user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contact_us_users/1
  # DELETE /contact_us_users/1.json
  def destroy
    @contact_us_user.destroy
    respond_to do |format|
      format.html { redirect_to contact_us_users_url, notice: 'Contact us user was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact_us_user
      @contact_us_user = ContactUsUser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_us_user_params
      params.permit(:email, :message, :phone_number, :company_name, :name)
    end
end
