class AppusersController < ApplicationController
   protect_from_forgery
   skip_before_action :authenticate_request
  before_action :set_appuser , only: [:show, :edit, :update, :destroy]
  #skip_before_action :authenticate_request

  def authenticate
    command = AuthenticateUser.call(params[:email], params[:password])

    if command.success?
      render json: { auth_token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end
  

  # GET /appusers
  # GET /appusers.json
  def index
    if (current_user.email == "aysyal31@gmail.com" || current_user.email == "vishwaas.malik@m.darcl.com")
     @appusers = Appuser.all
    else 
      redirect_to '/404.html'
    end
  end

  # GET /appusers/1
  # GET /appusers/1.json
  def show
  end



  # GET /appusers/new
  def new
    @appuser = Appuser.new
  end

  # GET /appusers/1/edit
  def edit 

    if (current_user.email == "aysyal31@gmail.com" )

    else 
      redirect_to '/404.html'
    end

  end

  # POST /appusers
  # POST /appusers.json
  def create
    @appuser = Appuser.new(appuser_params)

    respond_to do |format|
      if @appuser.save
        session[:user_id] = @appuser.id
        format.html { render  :verification , notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @appuser }
      else
        format.html { redirect_to '/login' ,notice: @appuser.errors }
        format.json { render json: @appuser.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /appusers/1
  # PATCH/PUT /appusers/1.json
  def update
    respond_to do |format|
      if @appuser.update(appuser_params)
        format.html { redirect_to '/profile', notice: 'Appuser was successfully updated.' }
        format.json { render :show, status: :ok, location: @appuser }
      else
        format.html { redirect_to '/profile/edit', notice: @appuser.errors }
        format.json { render json: @appuser.errors, status: :unprocessable_entity }
      end
    end
  end

  def verification 

  end

  # DELETE /appusers/1
  # DELETE /appusers/1.json
  def destroy
    if (current_user.email == "aysyal31@gmail.com" )

      @appuser.destroy
      respond_to do |format|
        format.html { redirect_to appusers_url, notice: 'Appuser was successfully destroyed.' }
        format.json { head :no_content }
      end
    else 
      redirect_to '/404.html'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_appuser
      @appuser = Appuser.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def appuser_params
      params.require(:appuser).permit( :email, :name, :phone_number,:password , :toc_accepted , :alt_phone_number , :bank_name , :bank_account_no ,:is_verified)
    end

     skip_before_action :authenticate_request

  def authenticate
    command = AuthenticateUser.call(params[:email], params[:password])

    if command.success?
      render json: { auth_token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end
end
