class ProfilesController < ApplicationController

def show 

end

def edit
    @appuser = current_user
  end

  def update
    @appuser = current_user
    if @appuser.update_attributes(user_params)
      redirect_to profile_url(), :notice  => "Successfully updated user."
    else
      render :edit
    end
  end

  private

    def user_params
      params.require(:user).permit(:name, :phone_number , :email , :password , :bank_name , :bank_account_no , :alt_phone_number )
    end

end

