json.array!(@contact_us_users) do |contact_us_user|
  json.extract! contact_us_user, :id, :email, :message, :phone_number, :company_name, :name
  json.url contact_us_user_url(contact_us_user, format: :json)
end
