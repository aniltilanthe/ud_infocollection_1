
json.(@trucker, :id, :trucker_name, :truck_number, :rc_number, :trucker_phn_no, :has_gps, :insurance, :pref_route, :fitness, :transit_pass, :permit, :emi, :opt_f1, :opt_f2, :opt_f3, :opt_f4, :opt_f5, :is_verified, :route_start, :route_end, :fleet_size, :truck_type, :created_at, :updated_at )

json.drivers @trucker.drivers, :driver_name, :license_no, :driver_pnh_no