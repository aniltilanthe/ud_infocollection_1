json.array!(@appusers) do |appuser|
  json.extract! appuser, :id, :username, :password_digest, :salt, :email, :name, :phone_number, :opt_f1, :opt_f2
  json.url appuser_url(appuser, format: :json)
end
