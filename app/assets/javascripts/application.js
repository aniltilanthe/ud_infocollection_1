// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require jquery1.js
//= require turbolinks
//= require jquery_nested_form
//= require bootstrap-sprockets
//= require jquery.easing.min.js
//= require bootstrap.min.js
//= require jqBootstrapValidation.js
//= require formValidation.js
//= require bootstrap.js
//= require agency.js
//= require contact_me.js




$(document).on('ready page:load', function () {
     
    
    $('#loginform')
        .formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                email: {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required and can\'t be empty'
                        },
                        emailAddress: {
                            message: 'The input is not a valid email address'
                        }
                    }
                }
            }
        });


    $('#signupform')
        .formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                "appuser[email]": {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required and can\'t be empty'
                        },
                        emailAddress: {
                            message: 'The input is not a valid email address'
                        }
                    }
                },
                "appuser[password]": {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and can\'t be empty'
                        },
                    stringLength: {
                        min: 6,
                        max: 15,
                        message: 'Password must be more than 6 and less than 15 characters long'
                        }
                    }
                },
                "appuser[name]": {
                    message: ' Name is not valid',
                    validators: {
                       notEmpty: {
                        message: 'Name is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'Name can only consist of alphabets'
                    }
                }
            },               
                "appuser[phone_number]": {
                 
                    validators: {
                    notEmpty: {
                        message: 'Phone no is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 10,
                        max: 10,
                        message: 'Phone number must be 10 characters long'
                        },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Not a valid IN phone number'
                    }                    


                    }   
                
            }, 

           "appuser[alt_phone_number]": {
                 validators: {
                    notEmpty: {
                        message: 'Phone no is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Not a valid IN phone number'
                    },
                    stringLength: {
                        min: 10,
                        max: 10,
                        message: 'Phone number must be 10 characters long'
                        }
                }
            },
            "appuser[bank_name]": {
                 validators: {
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'Name can only consist of alphabets'
                    }
                }
            },
             "appuser[bank_account_no]": {
                 validators: {
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Account no can consist of only digits'
                    },
                    stringLength: {
                        min: 10,
                        max: 16,
                        message: 'Bank account number must be more than 10 and less than 15 characters long'
                        }
                }
            },
            "appuser[toc_accepted]": {
                validators:{
                    notEmpty: {
                        message: 'Please click the checkbox'
                    }
                }
            }
            }
        
        });



$('.edit_appuser')
        .formValidation({
            message: 'This value is not valid',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                "appuser[email]": {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required and can\'t be empty'
                        },
                        emailAddress: {
                            message: 'The input is not a valid email address'
                        }
                    }
                },
                "appuser[password]": {
                    validators: {
                        notEmpty: {
                            message: 'The password is required and can\'t be empty'
                        },
                    stringLength: {
                        min: 6,
                        max: 15,
                        message: 'Password must be more than 6 and less than 15 characters long'
                        }
                    }
                },
                "appuser[name]": {
                    message: ' Name is not valid',
                    validators: {
                       notEmpty: {
                        message: 'Name is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'Name can only consist of alphabets'
                    }
                }
            },               
                "appuser[phone_number]": {
                 
                    validators: {
                    notEmpty: {
                        message: 'Phone no is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 10,
                        max: 10,
                        message: 'Phone number must be 10 characters long'
                        },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Not a valid IN phone number'
                    }                    


                    }   
                
            }, 

           "appuser[alt_phone_number]": {
                 validators: {
                    notEmpty: {
                        message: 'Phone no is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Not a valid IN phone number'
                    },
                    stringLength: {
                        min: 10,
                        max: 10,
                        message: 'Phone number must be 10 characters long'
                        }
                }
            },
            "appuser[bank_name]": {
                 validators: {
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'Name can only consist of alphabets'
                    }
                }
            },
             "appuser[bank_account_no]": {
                 validators: {
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Account no can consist of only digits'
                    },
                    stringLength: {
                        min: 10,
                        max: 16,
                        message: 'Bank account number must be more than 10 and less than 15 characters long'
                        }
                }
            },

            }
        
        });



    $('#new_trucker').formValidation({
        message: 'This value is not valid',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "trucker[truck_number]": {
                message: 'Truck Number is not valid',
                validators: {
                    notEmpty: {
                        message: 'Truck Number is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 9,
                        max: 15,
                        message: 'Truck Plate Number must be more than 10 and less than 15 characters long'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9 ]+$/,
                        message: 'The Truck Number can only consist of alphabetical, number, dot and underscore'
                    },
                    remote: {
                    	
                        name: 'id' ,
                        url: '/truckavailable',
                        
                        message: 'The Truck already exists in the database.',
                        //delay: 1000
                    }
                }
            },
            "trucker[trucker_name]": {
                message: 'Truck Name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Truck Name is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'The Trucker Name can only consist of alphabets'
                    }
                }
            },
            "trucker[trucker_phn_no]": {
                validators: {
                    notEmpty: {
                        message: 'Phone no is required and can\'t be empty'
                    },
                    phone: {
                        message: 'The input is not a valid IN phone number'
                    },
                    stringLength: {
                        min: 10,
                        max: 10,
                        message: 'Phone number must be 10 characters long'
                        }
                }
            },
            "trucker[fleet_size]": {
            	validators: {
                    notEmpty: {
                        message: 'Truck fleet is required and can\'t be empty'
                    },
            		digit: {
            			message: 'The input is not a valid digit'
            		}
            	}
            },
            "trucker[drivers_attributes][0][driver_name]": {
                message: 'Driver Name is not valid',
                validators: {
                    notEmpty: {
                        message: 'Driver Name is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'The Driver Name can only consist of alphabets'
                    }
                }
            },
            "trucker[drivers_attributes][0][driver_pnh_no]":  {
                validators: {
                    notEmpty: {
                        message: 'Phone no is required and can\'t be empty'
                    },
                    phone: {
                        message: 'The input is not a valid IN phone number'
                    },
                    stringLength: {
                        min: 10,
                        max: 10,
                        message: 'Phone number must be 10 characters long'
                        }
                }
            },
            "trucker[drivers_attributes][0][license_no]": {
                validators: {
                    notEmpty: {
                        message: 'License no is required and can\'t be empty'
                    }
                }
            },
            "trucker[transporter_name]": {
                validators: {
                    notEmpty: {
                        message: 'Transporter Name is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'The Transporter Name can only consist of alphabets'
                    }
                }
            },
            "trucker[truck_type]":{
                validators: {
                    notEmpty: {
                        message: 'Truck Type is required and can\'t be empty'
                    }

            } },
            "trucker[route_start]":{
                validators: {
                    notEmpty: {
                        message: 'Route Start is required and can\'t be empty'
                    }

            } },
            "trucker[route_end]":{
                validators: {
                    notEmpty: {
                        message: 'Route end is required and can\'t be empty'
                    }

            } },
            "trucker[consignee_name]": {
                validators: {
                    notEmpty: {
                        message: 'Consignee is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'The consignee name can only consist of alphabets'
                    }
                }
            },            
            "trucker[transporter_name]": {
                validators: {
                    notEmpty: {
                        message: 'Transporter is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z ]+$/,
                        message: 'The consignee name can only consist of alphabets'
                    }
                }
            },
            "trucker[transit_pass_attach]":{
                validators: {
                }

            },
            "trucker[rc_attach]":{
                validators: {
                    notEmpty: {
                        message: 'RC needs to be attached '
                    }
                }
            },
            "trucker[chassis_number_attach]":{
                validators: {
                }
            }

        }
    });

    $(window).scroll(function(){
        if ($(this).scrollTop() > 300){
            $('.navbar-default').addClass('navbar-shrink');
        }
        else{
            $('.navbar-default').removeClass('navbar-shrink');
        }
    });





});
