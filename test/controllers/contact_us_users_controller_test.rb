require 'test_helper'

class ContactUsUsersControllerTest < ActionController::TestCase
  setup do
    @contact_us_user = contact_us_users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:contact_us_users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create contact_us_user" do
    assert_difference('ContactUsUser.count') do
      post :create, contact_us_user: { company_name: @contact_us_user.company_name, email: @contact_us_user.email, message: @contact_us_user.message, name: @contact_us_user.name, phone_number: @contact_us_user.phone_number }
    end

    assert_redirected_to contact_us_user_path(assigns(:contact_us_user))
  end

  test "should show contact_us_user" do
    get :show, id: @contact_us_user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @contact_us_user
    assert_response :success
  end

  test "should update contact_us_user" do
    patch :update, id: @contact_us_user, contact_us_user: { company_name: @contact_us_user.company_name, email: @contact_us_user.email, message: @contact_us_user.message, name: @contact_us_user.name, phone_number: @contact_us_user.phone_number }
    assert_redirected_to contact_us_user_path(assigns(:contact_us_user))
  end

  test "should destroy contact_us_user" do
    assert_difference('ContactUsUser.count', -1) do
      delete :destroy, id: @contact_us_user
    end

    assert_redirected_to contact_us_users_path
  end
end
