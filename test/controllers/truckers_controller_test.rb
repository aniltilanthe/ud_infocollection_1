require 'test_helper'

class TruckersControllerTest < ActionController::TestCase
  setup do
    @trucker = truckers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:truckers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create trucker" do
    assert_difference('Trucker.count') do
      post :create, trucker: { driver_name: @trucker.driver_name, driver_phn_no: @trucker.driver_phn_no, emi: @trucker.emi, fitness: @trucker.fitness, has_gps: @trucker.has_gps, insurance: @trucker.insurance, license_no: @trucker.license_no, opt_f1: @trucker.opt_f1, opt_f2: @trucker.opt_f2, opt_f3: @trucker.opt_f3, opt_f4: @trucker.opt_f4, opt_f5: @trucker.opt_f5, permit: @trucker.permit, pref_route: @trucker.pref_route, rc_number: @trucker.rc_number, transit_pass: @trucker.transit_pass, truck_number: @trucker.truck_number, trucker_name: @trucker.trucker_name, trucker_phn_no: @trucker.trucker_phn_no }
    end

    assert_redirected_to trucker_path(assigns(:trucker))
  end

  test "should show trucker" do
    get :show, id: @trucker
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @trucker
    assert_response :success
  end

  test "should update trucker" do
    patch :update, id: @trucker, trucker: { driver_name: @trucker.driver_name, driver_phn_no: @trucker.driver_phn_no, emi: @trucker.emi, fitness: @trucker.fitness, has_gps: @trucker.has_gps, insurance: @trucker.insurance, license_no: @trucker.license_no, opt_f1: @trucker.opt_f1, opt_f2: @trucker.opt_f2, opt_f3: @trucker.opt_f3, opt_f4: @trucker.opt_f4, opt_f5: @trucker.opt_f5, permit: @trucker.permit, pref_route: @trucker.pref_route, rc_number: @trucker.rc_number, transit_pass: @trucker.transit_pass, truck_number: @trucker.truck_number, trucker_name: @trucker.trucker_name, trucker_phn_no: @trucker.trucker_phn_no }
    assert_redirected_to trucker_path(assigns(:trucker))
  end

  test "should destroy trucker" do
    assert_difference('Trucker.count', -1) do
      delete :destroy, id: @trucker
    end

    assert_redirected_to truckers_path
  end
end
