Rails.application.routes.draw do
  resources :contact_us_users
  resources :appusers
  resources :profiles
  resources :truckers do
    resources :drivers
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  

  # You can have the root of your site routed with "root"
   root 'welcome#welcome'


   post 'authentication' => 'authentication#authenticate'

   get 'welcome' => 'welcome#welcome'

   #get 'truckers/:trucker' => 'truckers#show'

   get 'trucker' => 'truckers#show'

   get 'upload'  => 'uploadtruckers#show'
   
   post 'upload' => 'uploadtruckers#submit'

   get 'profile' => 'profiles#show'

   get 'profile/edit' => 'profiles#edit'

   post 'profile/edit' => 'profile#update'


   get 'truckavailable' => 'truckers#truckavailable'

   get 'verification' => 'appusers#verification'


   #get 'truckers/:trucker[:truck_number]' => 'truckers#show'

  get '/login' => 'sessions#new'
  post '/login' => 'sessions#create'
  get '/logout' => 'sessions#destroy'

  get '/signup' => 'sessions#new'
  post '/signup' => 'appusers#create'
  post '/users' => 'appusers#create'

  get '/PrivacyPolicy' , :to => redirect('/privacyPolicy.html')

  get '/TermsOfUse' , :to => redirect('/TermsofUse.html')

  get '/UserAgreement' , :to => redirect('/UserAgreement.html')

  get '/Contact' , :to => redirect('/contact.html')



  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
