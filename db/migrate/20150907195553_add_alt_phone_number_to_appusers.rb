class AddAltPhoneNumberToAppusers < ActiveRecord::Migration
  def change
    add_column :appusers, :alt_phone_number, :string
  end
end
