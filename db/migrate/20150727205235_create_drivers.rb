class CreateDrivers < ActiveRecord::Migration
  def change
    create_table :drivers do |t|
      t.string :driver_name
      t.string :driver_pnh_no
      t.string :license_no
      t.references :trucker, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
