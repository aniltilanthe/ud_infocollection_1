class AddBankAccountNoToAppusers < ActiveRecord::Migration
  def change
    add_column :appusers, :bank_account_no, :string
  end
end
