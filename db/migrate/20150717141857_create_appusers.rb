class CreateAppusers < ActiveRecord::Migration
  def change
    create_table :appusers do |t|
      t.string :username
      t.string :crypted_password
      t.string :salt
      t.string :email
      t.string :name
      t.string :phone_number
      t.string :opt_f1
      t.string :opt_f2

      t.timestamps null: false
    end
  end
end
