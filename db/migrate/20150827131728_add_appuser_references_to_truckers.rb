class AddAppuserReferencesToTruckers < ActiveRecord::Migration
  def change
    add_reference :truckers, :appuser, index: true, foreign_key: true
  end
end
