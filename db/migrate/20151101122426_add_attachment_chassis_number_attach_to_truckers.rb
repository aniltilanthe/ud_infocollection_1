class AddAttachmentChassisNumberAttachToTruckers < ActiveRecord::Migration
  def self.up
    change_table :truckers do |t|
      t.attachment :chassis_number_attach
    end
  end

  def self.down
    remove_attachment :truckers, :chassis_number_attach
  end
end
