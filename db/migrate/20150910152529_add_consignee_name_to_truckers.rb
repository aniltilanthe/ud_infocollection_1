class AddConsigneeNameToTruckers < ActiveRecord::Migration
  def change
    add_column :truckers, :consignee_name, :string
  end
end
