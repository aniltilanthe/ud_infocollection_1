class CreateContactUsUsers < ActiveRecord::Migration
  def change
    create_table :contact_us_users do |t|
      t.string :email
      t.text :message
      t.string :phone_number
      t.string :company_name
      t.string :name

      t.timestamps null: false
    end
  end
end
