class AddIsVerifiedToAppusers < ActiveRecord::Migration
  def change
    add_column :appusers, :is_verified, :boolean
  end
end
