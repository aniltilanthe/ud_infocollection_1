class AddTocAcceptedToAppusers < ActiveRecord::Migration
  def change
    add_column :appusers, :toc_accepted, :boolean
  end
end
