class AddTransporterNameToTruckers < ActiveRecord::Migration
  def change
    add_column :truckers, :transporter_name, :string
  end
end
