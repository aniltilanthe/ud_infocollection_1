class AddIndexToAppusers < ActiveRecord::Migration
  def change
    add_index :appusers, :email, unique: true
  end
end
