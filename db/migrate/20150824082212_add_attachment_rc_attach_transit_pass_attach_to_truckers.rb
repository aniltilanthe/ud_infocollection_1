class AddAttachmentRcAttachTransitPassAttachToTruckers < ActiveRecord::Migration
  def self.up
    change_table :truckers do |t|
      t.attachment :rc_attach
      t.attachment :transit_pass_attach
      t.attachment :chassis_number_attach
    end
  end

  def self.down
    remove_attachment :truckers, :rc_attach
    remove_attachment :truckers, :transit_pass_attach
    remove_attachment :truckers, :chassis_number_attach
  end
end
