class CreateTruckers < ActiveRecord::Migration
  def change
    create_table :truckers do |t|
      t.string :trucker_name
      t.string :truck_number
      t.string :rc_number
      t.string :trucker_phn_no
      t.boolean :has_gps
      t.string :insurance
      t.string :pref_route
      t.string :fitness
      t.string :transit_pass
      t.string :permit
      t.string :emi
      t.string :opt_f1
      t.string :opt_f2
      t.string :opt_f3
      t.string :opt_f4
      t.string :opt_f5
      t.boolean :is_verified
      t.string :route_start
      t.string :route_end
      t.integer :fleet_size
      t.string :truck_type

      t.timestamps null: false
    end
    add_index :truckers, :truck_number, unique: true
  end
end
