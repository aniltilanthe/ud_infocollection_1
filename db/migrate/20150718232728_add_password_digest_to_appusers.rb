class AddPasswordDigestToAppusers < ActiveRecord::Migration
  def change
  	add_column :appusers, :password_digest, :string
  end
end
