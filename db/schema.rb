# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151107110923) do

  create_table "appusers", force: :cascade do |t|
    t.string   "username",         limit: 255
    t.string   "crypted_password", limit: 255
    t.string   "salt",             limit: 255
    t.string   "email",            limit: 255
    t.string   "name",             limit: 255
    t.string   "phone_number",     limit: 255
    t.string   "opt_f1",           limit: 255
    t.string   "opt_f2",           limit: 255
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "password_digest",  limit: 255
    t.integer  "truckers_count",   limit: 4,   default: 0
    t.boolean  "toc_accepted"
    t.string   "bank_name",        limit: 255
    t.string   "alt_phone_number", limit: 255
    t.string   "bank_account_no",  limit: 255
    t.boolean  "is_verified"
  end

  add_index "appusers", ["email"], name: "index_appusers_on_email", unique: true, using: :btree

  create_table "contact_us_users", force: :cascade do |t|
    t.string   "email",        limit: 255
    t.text     "message",      limit: 65535
    t.string   "phone_number", limit: 255
    t.string   "company_name", limit: 255
    t.string   "name",         limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "drivers", force: :cascade do |t|
    t.string   "driver_name",   limit: 255
    t.string   "driver_pnh_no", limit: 255
    t.string   "license_no",    limit: 255
    t.integer  "trucker_id",    limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  add_index "drivers", ["trucker_id"], name: "index_drivers_on_trucker_id", using: :btree

  create_table "truckers", force: :cascade do |t|
    t.string   "trucker_name",                       limit: 255
    t.string   "truck_number",                       limit: 255
    t.string   "rc_number",                          limit: 255
    t.string   "trucker_phn_no",                     limit: 255
    t.boolean  "has_gps"
    t.string   "insurance",                          limit: 255
    t.string   "pref_route",                         limit: 255
    t.string   "fitness",                            limit: 255
    t.string   "transit_pass",                       limit: 255
    t.string   "permit",                             limit: 255
    t.string   "emi",                                limit: 255
    t.string   "opt_f1",                             limit: 255
    t.string   "opt_f2",                             limit: 255
    t.string   "opt_f3",                             limit: 255
    t.string   "opt_f4",                             limit: 255
    t.string   "opt_f5",                             limit: 255
    t.boolean  "is_verified"
    t.string   "route_start",                        limit: 255
    t.string   "route_end",                          limit: 255
    t.integer  "fleet_size",                         limit: 4
    t.string   "truck_type",                         limit: 255
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "rc_attach_file_name",                limit: 255
    t.string   "rc_attach_content_type",             limit: 255
    t.integer  "rc_attach_file_size",                limit: 4
    t.datetime "rc_attach_updated_at"
    t.string   "transit_pass_attach_file_name",      limit: 255
    t.string   "transit_pass_attach_content_type",   limit: 255
    t.integer  "transit_pass_attach_file_size",      limit: 4
    t.datetime "transit_pass_attach_updated_at"
    t.integer  "appuser_id",                         limit: 4
    t.string   "transporter_name",                   limit: 255
    t.string   "consignee_name",                     limit: 255
    t.string   "chassis_number_attach_file_name",    limit: 255
    t.string   "chassis_number_attach_content_type", limit: 255
    t.integer  "chassis_number_attach_file_size",    limit: 4
    t.datetime "chassis_number_attach_updated_at"
  end

  add_index "truckers", ["appuser_id"], name: "index_truckers_on_appuser_id", using: :btree
  add_index "truckers", ["truck_number"], name: "index_truckers_on_truck_number", unique: true, using: :btree

  create_table "uploadtruckers", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "drivers", "truckers"
  add_foreign_key "truckers", "appusers"
end
